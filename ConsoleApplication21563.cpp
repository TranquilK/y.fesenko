#include <stdio.h>
#include "stdafx.h"
#include <locale.h>


int a[400];
int main()
{
	setlocale(LC_ALL, "Rus");
	int i, n, t, m = 1000, j;
	
	n = 1;
	a[0] = 1;
	for (j = 0; j<m; j++)
	{
		for (i = 0; i<n; i++)
			a[i] *= 2;
		for (t = 0; t<n; t++)
			if (a[t]>9)
				a[t] %= 10, ++a[t + 1];
		if (a[n])
			++n;
	}
	n += 10;
	while (!a[n])
		--n;
	int sum=0;
	for (i = n; i >= 0; i--) {
		sum = sum + a[i];
	}
	printf("Сумма цифр числа 2^1000 равна %d.\n", sum);
	return 0;
}